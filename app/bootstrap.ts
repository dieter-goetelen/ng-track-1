﻿require('bootstrap/dist/css/bootstrap.css');

import * as angular from 'angular';
import { RootController } from './root.controller';
import { MailController } from './mail.controller';
import { MailService } from './mail.service';
import { UserService } from './user.service';

export default angular.module('app', [])
    .service(MailService.iid, MailService)
    .service(UserService.iid, UserService)
    .controller(MailController.iid, MailController)
    .controller(RootController.iid, RootController);