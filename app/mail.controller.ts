﻿import { UserService } from './user.service';
import { MailService } from './mail.service';
import { Mail } from './mail';

export class MailController {
    mails: Array<Mail>;
    selectedMail: Mail;
    pageState = {
        newMode: false,
        replyMode: false,
        readMode: true
    };
    newMail: Mail;

    static $inject = [MailService.iid, '$log', UserService.iid, '$interval'];
    static iid: string = 'MailController';

    constructor(private mailService: MailService, private $log: ng.ILogService, private userService: UserService, private $interval: ng.IIntervalService) {
        this.initialize().then(mails => {
            if (mails && mails.length > 0) {
                this.setSelectedTo(mails[0]);
            }

            $log.info('Mailcontroller initialized');
        });
    }

    resetNewMail() {
        this.newMail = {
            type: '',
            description: '',
            title: ''
        }
    }

    gotoNewMail(form: ng.IFormController) {
        form.$setPristine();
        this.setPageState(true, false, false);
        this.resetNewMail();
    }

    saveMail(form: ng.IFormController, mail: Mail) {
        if (form.$invalid) {
            return;
        }
        mail.type = 'send';
        this.mailService.saveMail(mail).then((m: Mail) => {
            this.mails.unshift(m);
            this.setSelectedTo(m);
            this.setPageState(false, false, true);

            // resets the form
            form.$setPristine();
        });
    }

    setMails() {
        return this.mailService.getMails().then(mails => {
            this.mails = mails || [];
            return mails;
        });
    }
    initialize() {
        this.$interval(this.setMails.bind(this), 10000);
        return this.setMails()     }

    setSelectedTo(mail: Mail) {
        this.selectedMail = mail;
    }

    setPageState(newMode: boolean, replyMode: boolean, readMode: boolean) {
        this.pageState = { newMode, replyMode, readMode };
    }

}