﻿import { Mail } from './mail';
import { UserService } from './user.service';
import * as angular from 'angular';

const mails: Array<Mail> = [
    {
        id: "1",
        type: 'received',
        title: 'RE: Angular2 course',
        description: 'Is there any free space for the Angular 2 course?',
        from: "jane@frost.com"

    },
    {
        id: "2",
        type: 'send',
        title: 'fw: Angular2 course',
        description: 'Is there any free space for the Angular 2 course?',
        to: 'john@doe.com'
    }
];

export class MailService {
    static iid: string = 'MailService';
    static $inject = ['$q', '$http', UserService.iid];

    endpoint: string = 'https://rd-mail.firebaseio.com/mails';
    user: string;
    constructor(
        private $q: ng.IQService,
        private $http: ng.IHttpService,
        private userService: UserService
    ) {
        this.user = userService.name;
    }

    private mapResultToArray(result: any) {
        let returnValue: Array<Mail> = [];

        for (let item in result.data) {
            if (result.data.hasOwnProperty(item)) {
                let mailItem: Mail = result.data[item];
                mailItem.id = item;
                returnValue.push(mailItem);
            }
        }

        return returnValue.reverse();
    }

    public getMails() {
        //return this.$q.resolve(angular.copy(mails));
        return this.$http.get(this.endpoint + `/${this.user}.json`).then(result => this.mapResultToArray(result));
    }

    public getMailById(id: string) {
        //let mail: Mail = mails.filter(m => m.id === id)[0];
        // return this.$q.resolve(mail);
        return this.getMails().then(mails => mails.filter(m => m.id === id)[0]);
    }
    public saveMail(mail: Mail) {
        let receivedMail = angular.copy(mail);

        receivedMail.to = null;
        receivedMail.from = this.user;
        receivedMail.type = 'received';

        return this.$q.all([
            this.$http.post(`${this.endpoint}/${mail.to}.json`, receivedMail),
            this.$http.post(`${this.endpoint}/${this.user}.json`, mail)
        ]).then((result: Array<ng.IHttpPromiseCallbackArg<{}>>) => {
            let id: string = (<any>result[1].data).name;
            
            mail.id = id;

            return mail;
        });


        // return this.$http.post(this.endpoint, mail).then(result => (<Mail>result.data));
        //mail.id = mails[mails.length - 1].id + 1;

        //mails.push(mail);
        //return this.$q.resolve(mail.id);
    }
}