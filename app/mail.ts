﻿export interface Mail {
    id?: string;
    //id?: number,
    title: string,
    description: string,
    type: string,
    to?: string;
    from?: string;
}